<?php
// define variables and set to empty values
$nameErr = $orgErr = $designationErr = $emailErr = $phoneErr = $genderErr = $websiteErr = "";
$name = $organisation = $designation = $email = $phone = $message = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
   if (empty($_POST["name"])) {
     $nameErr = "Name is required";
   } else {
     $name = test_input($_POST["name"]);
     // check if name only contains letters and whitespace
     if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
       $nameErr = "Only letters and white space allowed";
     }
   }

   if (empty($_POST["organisation"])) {
     $orgErr = "Organisation is required";
   } else {
     $organisation = test_input($_POST["organisation"]);
     // check if name only contains letters and whitespace
     if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
       $orgErr = "Only letters and white space allowed";
     }
   }

    if (empty($_POST["designation"])) {
      $designationErr = "Designation is required";
    } else {
      $designation = test_input($_POST["designation"]);
      // check if name only contains letters and whitespace
      if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
        $designationErr = "Only letters and white space allowed";
      }
    }

   if (empty($_POST["email"])) {
     $emailErr = "Email is required";
   } else {
     $email = test_input($_POST["email"]);
     // check if e-mail address is well-formed
     if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
       $emailErr = "Invalid email format";
     }
   }

   if (empty($_POST["phone"])) {
     $phoneErr = "Phone is required";
   } else {
     $phone = test_input($_POST["email"]);
     // check if e-mail address is well-formed
     if (!preg_match("/^[0-9 ]*$/",$phone)) {
       $phoneErr = "Invalid Phone Number format";
     }
   }

   if (empty($_POST["message"])) {
     $comment = "";
   } else {
     $comment = test_input($_POST["message"]);
   }
}

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

$to = "syed@digitribe.in";
$subject = 'Mail Form krayon Website';
$message = "Name : " .$name ."\r\n" . "Organisation : " .$organisation ."\r\n" . "Designation : " .$designation ."\r\n" . "Email: " .$email ."\r\n" . "Phone No : " .$phone ."\r\n" . "Message : " $comment;

$headers = array("From: info@lxl.in", "Reply-To: info@lxl.in", "X-Mailer: PHP/".PHP_VERSION);
$headers = implode("\r\n", $headers);

mail($to, $subject, $message, $headers);
?>
OK

'use strict';
var app = angular.module('krayon-events', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider){

		 $urlRouterProvider.otherwise('');
		 $stateProvider
		 .state('eventview', {
				url : '/eventview/:text',
				templateUrl : 'eventview.html'
				// controller: 'viewpagectrl',
			})
			.state('home', {
 				url : '',
 				templateUrl : 'home.html'
 			});
			// $locationProvider.html5Mode(true).hashPrefix('!')
		});

app.controller('eventCtrl', ['$scope', '$http', '$state',
	function($scope,$http,$state){
		$scope.events= [
					{id:0, photo:'dellchamp.png', text:"Dell Champs", cities:"Madurai, Coimbatore, Hubli - Dharwad, Mangalore, Vizag, Vijayawada, Jamshedpur, Bhubaneshwar, Lucknow, Nagpur, Indore, Pune, Raipur, Ahmedabad, Vadodara, Amritsar, Ludhiana, Chandigarh, Nagpur & Jaipur", target:"5th to 7th Standard", category:"A+ and A category of Schools", reach:"400 schools, 40000 Students & 100000 Parents", timeline:"July 2014- September 2014", bgcolor:"#FFDC1E"},
					{id:1, photo:'eventlogo35.png', text:"POGO AMAZING KIDS AWARDS", cities:"Mumbai, Delhi, Bangalore, Hyderabad, Chennai, Jaipur, Ludhiana, Amritsar & Chandigarh", target:"	3rd to 8th Std", category:"SEC A & B schools", reach:"990 schools - 10,00,000 students", timeline:"", bgcolor:"#F02F72"},
					{id:2, photo:'eventlogo13.png', text:"Ideas for India", cities:"Contest open to the entire country On ground activation held in 30 cities across India: Delhi, Chandigarh, Mumbai, Ahmedabad, Hyderabad, Kolkata, Lucknow, Chennai, Guwahati, Bangalore, Jaipur, Saharanpur, Agra, Kanpur, Varanasi, Bareilly, Pune, Nasik, Nagpur, Indore, Bhopal, Coimbatore, Madurai, Kochi, Mysore, Vishakapatnam, Guntur, Patna, Bhubaneshwar, Shillong", target:"14 to 21 years of age", category:"Open to the entire nation aged between 14 to 21 years of age", reach:"Over 25 lakh youth across the nation", timeline:"September 2011 - December 2011", bgcolor:"#EEEEEE"},
					{id:3, photo:'eventlogo36.png', text:"Pogo Funtakshari", cities:"Delhi, Mumbai, Kolkata, Bangalore & Chennai", target:"4th to 9th Std", category:"SEC A, B & C schools", reach:"1200 schools - 12,00,000 students", timeline:"August - September", bgcolor:"#FF6302"},
					{id:4, photo:'eventlogo10.png', text:"NSE-Funancial Quest 2013", cities:"15 cities across india (Ahmedabad / Baroda, Bangolore, Bhubaneshwar, Chandigarh, Coimbatore, Dehradun, Guwahathi, Hyderabad, Indore, Jaipur, Kochi, Lucknow, Nagpur, Pune, Surat)", target:"08th & 09th Standard", category:"A+, A & B Category", reach:"200 schools & over 60000 students", timeline:"September 2013 to March 2014", bgcolor:"#B4DA13"},
					{id:5, photo:'eventlogo1.png', text:"Zee Learn Magic with Aladdin", cities:"Ahmedabad, Surat, Baroda, Rajkot & Pune", target:"2nd to 6th Std", category:"SEC B & C", reach:"600 Schools - 600,000 students", timeline:"December 2007", bgcolor:"#9E6FCF"},
					// {id:6, photo:'SchoolAuditions.png', text:"School Auditions", cities:"Agra , Allahabad, Lucknow, Kanpur, Meerut & Varanasi", target:"	4th to 8th Std.", category:"SEC A, B, C & govt schools (Eng & Hindi medium)", reach:"360 schools - 360,000 students", timeline:"April - May 2008", bgcolor:"#CEE45A"},
					{id:6, photo:'eventlogo19.png', text:"Klenza", cities:"Bangalore", target:"Std 4th-10th", category:"A+,A & B+", reach:"20 schools , 10,000 students", timeline:"June- July 2014", bgcolor:"#01B8F4"},
					{id:7, photo:'eventlogo2.png', text:"BOOST BOWLING CLINIC BY BRETT LEE", cities:"Mumbai & Bangalore", target:"Above 6 years", category:"", reach:"", timeline:"", bgcolor:"#DC251F"},
					{id:8, photo:'eventlogo0.png', text:"Actigrow Health Campaign", cities:"23", target:"", category:"", reach:"3750 schools, 225,000 children", timeline:"", bgcolor:"#FFFFFF"},
					{id:9, photo:'eventlogo41.png', text:"Anemia Free Anaemia Free India Campaign - Tata Salt Plus", cities:"Delhi, Lucknow, Kanpur, Chennai, Bangalore", target:"4 years - 13 years ", category:"A, A+, B+ ", reach:"", timeline:"", bgcolor:"#F02F72"},
					{id:10, photo:'eventlogo4.jpg', text:"Cartoon Network Chowder Fun Food Funda", cities:"Mumbai, Delhi, Ahmedabad, Surat, Baroda, Kolkata, Pune, Lucknow, Kanpur, Hyderabad, Ludhiana, Amritsar, Jaipur, Bengaluru", target:"4 years - 13 years", category:"A, B", reach:"1350 schools - 250000 childrens", timeline:"", bgcolor:"#FFFFFF"},
					{id:11, photo:'eventlogo3.png', text:"CN SCHOOL OF SELF DEFENSE", cities:"Mumbai, Bangalore, Delhi, Coimbatore, Hyderabad, Kolkata, Varanasi, Ahmedabad and Lucknow", target:"1st to 5th Std.", category:"A,B", reach:"1350 schools, 1300000 childrens", timeline:"", bgcolor:"#FF6302"},
					{id:12, photo:'eventlogo11.jpg', text:"Gattu - The Movie", cities:"", target:"", category:"", reach:"", timeline:"", bgcolor:"#0D8FAF"},
					{id:13, photo:'eventlogo43.png', text:"WIZKIDS", cities:" India, Pakistan, Sri Lanka, Nepal, UAE,  Bangladesh", target:"1st to 12th Std ", category:"A+, A, B+, B, C+, C", reach:"2075000 childrens", timeline:"2002-2015", bgcolor:"#9E6FCF"},
					{id:14, photo:'eventlogo12.jpg', text:"HUL KNORR SOUPY NOODLES KHAO PEEO KHELO CHAMPIONSHIP", cities:"", target:"1st - 6th STD", category:"A,B", reach:"1700 schools, 1020000 childrens ", timeline:"2011", bgcolor:"#FFFFFF"},
					{id:15, photo:'eventlogo21.png', text:"INTEL SCP", cities:"Nasik, Pune, Nagpur, Indore, Baroda, Ahmedabad", target:"6th to 9th Std", category:"B, Govt.", reach:"60 schools, 40000 childrens", timeline:"", bgcolor:"#01B8F4"},
					//{id:17, photo:'IdeasforIndia.png', text:"CLASSMATE IDEAS FOR INDIA CHALLENGE", cities:"Delhi, Chandigarh, Mumbai, Ahmedabad, Hyderabad, Kolkata, Lucknow, Chennai, Guwahati, Bangalore, Jaipur, Saharanpur, Agra, Kanpur, Varanasi, Bareilly, Pune, Nasik, Nagpur, Indore, Bhopal, Coimbatore, Madurai, Kochi, Mysore, Vishakapatnam, Guntur, Patna, Bhubaneshwar, Shillong", target:"7 STD + ", category:"Open", reach:"60,040 childrens + young adults ", timeline:"2011", bgcolor:"#DC251F"},
					{id:16, photo:'eventlogo44.jpg', text:"YOUNG AUTHOR CONTEST", cities:"34", target:"5th to 12th Std ", category:"A,B,C", reach:"6000 schools, 6000000 childrens", timeline:"", bgcolor:"#FFFFFF"},
					// {id:20, photo:'', text:"ITC CLASSMATE IDEAS FOR INDIA", cities:"Delhi, Chandigarh, Mumbai, Ahmedabad, Hyderabad, Kolkata, Lucknow, Chennai, Guwahati, Bangalore, Jaipur, Saharanpur, Agra, Kanpur, Varanasi, Bareilly, Pune, Nasik, Nagpur, Indore, Bhopal, Coimbatore, Madurai, Kochi, Mysore, Vishakapatnam, Guntur, Patna, Bhubaneshwar, Shillong", target:"14 to 21 years", category:"", reach:"60039 childrens", timeline:"2011", bgcolor:"#EEEEEE"},
					{id:17, photo:'eventlogo16.png', text:"JUNIOR HORLICKS DAY OUT", cities:"Kolkata & Bangalore", target:"2 to 5 years", category:"", reach:"2000 childrens", timeline:"", bgcolor:"#FF6302"},
					{id:18, photo:'eventlogo14.png', text:"INFOSYS YOUNG ACHIEVERS AWARD", cities:"36", target:"", category:"", reach:"10000 childrens", timeline:"", bgcolor:"#EEEEEE"},
					{id:19, photo:'eventlogo18.png', text:"KELLOGGS BREAKFAST CHAMPS", cities:"Kolkata, Chennai, Delhi & Mumbai", target:"3rd to 8th STD", category:"A+ and A", reach:"Over 100000 childrens", timeline:"2013", bgcolor:"#B4DA13"},
					{id:20, photo:'eventlogo46.png', text:"KNORR SOUPY NOODLES KHAO PEEYO GAO", cities:"20 cities across India", target:"1st to 6th Std", category:"SEC A & B", reach:"1700 schools, 1020000 childrens ", timeline:"", bgcolor:"#008641"},
					{id:21, photo:'eventlogo38.png', text:"NACH LE VE WITH SAROJ KHAN", cities:"Agra , Allahabad, Lucknow, Kanpur, Meerut & Varanasi", target:"4th to 8th STD", category:"SEC A, B, C & govt schools ", reach:"360 schools, 360000 childrens", timeline:"2008", bgcolor:"#CEE45A"},
					{id:22, photo:'eventlogo24.png', text:"NICK BIG GREEN HELP", cities:"Mumbai, Delhi & Bangalore", target:" 2nd to 8th Std", category:"A&B", reach:"450 schools, 1000 childrens", timeline:"", bgcolor:"#01B8F4"},
					{id:23, photo:'eventlogo37.png', text:"SAHARA - CHACHA CHOUDARY WRITE AN EPISODE CONTEST", cities:"Mum, Kol, Del, Ludh, Luck, Ahd, Ind & Bhop", target:"1st to 12th Std.", category:"A, B & C", reach:"800 schools, 800000 childrens", timeline:"2005", bgcolor:"#EEEEEE"},
					{id:24, photo:'eventlogo30.png', text:"SUNFEAST MILKY MAGIC ALL-ROUNDER", cities:"Chennai, Madurai, Salem, Dindigul, Erode, Trichy, Thirpur, Coimbatore, Vellore and Pondicherry", target:"4th to 9th std", category:"A,B & C", reach:"1440 schools, 7000000 childrens", timeline:"2010", bgcolor:"#FFDC1E"},
					{id:25, photo:'eventlogo42.png', text:"TCS TEACHERS AWARDS", cities:"41 cities across the nation", target:"5th to 12th STD", category:"", reach:"5000 schools", timeline:"", bgcolor:"#F02F72"},
					{id:26, photo:'eventlogo7.png', text:"ESSENCE LEARNING STYLE TEST", cities:"Gujarat & Maharashtra", target:"1st to 9th STD", category:"", reach:"100 schools, 400 t0 600 childrens", timeline:"", bgcolor:"#FFDC1E"},
					{id:27, photo:'eventlogo15.jpg', text:"JUNIOR HORLICKS GROWTH ASSESSMENT PROGRAM", cities:"Bangalore, Chennai, Hyderabad, Madurai, Cochin, Bhubaneswar & Kolkata", target:"2 to 6 years", category:" A,B & C", reach:"400 schools, 40000 childrens", timeline:"2013", bgcolor:"#FFFFFF"},
					{id:28, photo:'eventlogo17.png', text:"STAR SPORTS MY DEBUT MATCH", cities:"Mumbai, Mohali, Bengaluru, Nagpur & Delhi", target:"8 to 15 years", category:"A+, A, B+ & B", reach:"3225 schools", timeline:"2015", bgcolor:"#B4DA13"},
					{id:29, photo:'eventlogo7.png', text:"NAT GEO YOUNG ASTRONAUTS", cities:"Delhi, Mumbai, Chennai & Bangalore ", target:"5th to 8th STD", category:"A & A+ ", reach:"60 schools, 350 childrens", timeline:"2011", bgcolor:"#9E6FCF"},
					{id:30, photo:'eventlogo33.jpg', text:"NATIONAL GEOGRAPHIC GREEN PLANET", cities:"Mumbai, Bangalore, Chennai, Delhi & Kolkata ", target:"6th to 9th std", category:"SEC A", reach:"50 schools, 1000 childrens", timeline:"", bgcolor:"#CEE45A"},
					{id:31, photo:'eventlogo49.png', text:"NICK MOTU PATLU KI JODI", cities:"Mumbai, Kolkata, Delhi, Allahabad, Varanasi, Lucknow & Kanpur", target:"3rd to 7th std", category:"Sec A & B ", reach:"1200 schools, 750 childrens", timeline:"", bgcolor:"#FFFFFF"},
					{id:32, photo:'eventlogo34.png', text:"NSE SEASON 5", cities:"Amritsar, Chandigarh, Dehradun, Lucknow, Kolkata, Shillong, Vizag, Bangalore, Coimbatore, Trivandrum, Surat, Baroda, Bhopal, Nagpur & Pune", target:"8th to 9th STD", category:"", reach:"225 schools, 200 childrens", timeline:"2015", bgcolor:"#DC251F"},
					{id:33, photo:'eventlogo7.png', text:"PKL SHADOW CAMPAIGN", cities:"Mumbai, Nashik, Kolhapur, Pune, Nagpur, Aurangabad, Hubli, Mangalore, Mysore, Bangalore, Hyderabad, Vizag, Warangal ,Vijaywada", target:"U- 17", category:"", reach:"2000 schools, 1.2 M childrens", timeline:"", bgcolor:"#FFDC1E"},
					{id:34, photo:'eventlogo50.png', text:"POGO BHEEM KI TEAM", cities:"New Delhi, Mumbai, Kolkata, Bengaluru, Ahmedabad, Indore, Bhopal, Lucknow, Kanpur and Ahmedabad", target:"", category:"", reach:"500 schools", timeline:"2010", bgcolor:"#F02F72"},
					{id:35, photo:'eventlogo47.png', text:"PROFORCE EVENT", cities:"Mumbai, Navi Mumbai, Thane, Pune, Kolhapur, Baroda, Kolkata, Kerala, Delhi, Gurgaon, NCR, Ghandhinager, Ahmedabad, Guwahati.", target:"6-14 YRS", category:"A+, A, B+, B ", reach:"1000 schools", timeline:"", bgcolor:"#EEEEEE"},
					{id:36, photo:'eventlogo26.png', text:"SONY BRIEFING ", cities:"Ahmedabad, Vadodara, Surat, Rajkot, Bhavnagar & Jamnagar", target:"1st to 8th STD", category:"A+, A & B", reach:"160 schools, 8000 childrens", timeline:"2013", bgcolor:"#FF6302"},
					{id:37, photo:'eventlogo7.png', text:"YOUNG VOICES OF BIHAR", cities:"Bihar", target:"8th to 12th STD", category:"A, B & C", reach:"100 schools, 100000 childrens", timeline:"2006", bgcolor:"#B4DA13"},
					{id:38, photo:'eventlogo45.png', text:"YOUTUBE SPACE LAB", cities:"26 cities across India", target:"Std. 9 to 12", category:"SEC A & B+ ", reach:"830 schools, 250 childrens", timeline:"2011", bgcolor:"#9E6FCF"},
					{id:39, photo:'eventlogo48.png', text:"ZUCI JUNIOR_2014", cities:"Delhi NCR, Mumbai & Bangalore", target:"1st to 7th STD", category:"", reach:"600 schools, 3000000 childrens", timeline:"2014", bgcolor:"#CEE45A"},


			];
			$scope.block = false;
			$scope.hoverIn = function(){
				this.block = true;
			 };

			 $scope.hoverOut = function(){
				this.block = false;
			 };

			 $scope.galleryimages = [
		        	{actigrowth : ["Picture1.jpg", "Picture2.jpg", "Picture3.jpg" , "Picture4.jpg" , "Picture5.jpg"]},
		        	{horlickswizkids : ["Picture1.png", "Picture2.jpg", "Picture3.jpg" , "Picture4.jpg" , "Picture5.jpg" , "Picture6.jpg" , "Picture7.jpg" , "Picture8.jpg" , "Picture9.jpg"]},
							{dell : ["dell0.jpg", "dell1.jpg", "dell2.jpg", "dell3.jpg", "dell4.jpg", "dell5.jpg", "dell6.jpg", "dell7.jpg", "dell8.jpg", "dell9.jpg"]},
							{nsepicture : ["nse0.jpg", "nse1.jpg", "nse2.jpg", "nse3.jpg", "nse4.jpg"]},
							{nsepress : ["nse001.jpg", "nse002.jpg", "nse003.jpg", "nse004.jpg"]},
							{boostpicture : ["boost0.jpg", "boost1.jpg", "boost2.jpg", "boost3.jpg"]},
							{boostpress : ["boost001.jpg", "boost002.jpg", "boost003.jpg", "boost004.jpg", "boost005.jpg"]},
							{aladdin : ["aladdin0.jpg", "aladdin1.jpg", "aladdin2.jpg", "aladdin3.jpg", "aladdin4.jpg", "aladdin5.jpg", "aladdin6.jpg", "aladdin7.jpg"]},
							{klenza : ["klenza0.jpg", "klenza1.jpg", "klenza2.jpg", "klenza3.jpg", "klenza4.jpg", "klenza5.jpg", "klenza6.jpg", "klenza7.jpg",]},
							{pogo : ["pogo0.jpg", "pogo1.jpg", "pogo2.jpg", "pogo3.jpg", "pogo4.jpg" ]},
							{school : ["school0.jpg", "school1.jpg", "school2.jpg", "school3.jpg", "school4.jpg", "school5.jpg"]},
							{kids : ["kids0.jpg", "kids1.jpg", "kids2.jpg", "kids3.jpg", "kids4.jpg" ]},
							{ideas : ["ideas0.jpg", "ideas1.jpg", "ideas2.jpg", "ideas3.jpg", "ideas4.jpg", "ideas5.jpg", "ideas6.jpg", "ideas7.jpg",]},
							{hulknorr : ["hul0.jpg", "hul1.jpg", "hul3.jpg", "hul3.jpg", "hul4.jpg", ]},
							{cnchowder : ["cnchowder1.jpg", "cnchowder2.jpg", "cnchowder3.jpg", "cnchowder4.jpg", "cnchowder5.jpg", "cnchowder6.jpg",]},
							{cnschool : ["cnschool1.jpg", "cnschool2.jpg", "cnschool3.jpg", "cnschool4.jpg", "cnschool5.jpg", "cnschool6.jpg", "cnschool7.jpg"]},
							{Infosysyoung : ["Infosysyoung1.jpg", "Infosysyoung2.jpg", "Infosysyoung3.jpg", "Infosysyoung4.jpg", "Infosysyoung5.jpg"]},
							{horlicksgap: ["horlicks_gap1.jpg", "horlicks_gap2.jpg", "horlicks_gap3.jpg", "horlicks_gap4.jpg", "horlicks_gap5.jpg", "horlicks_gap6.jpg", "horlicks_gap7.jpg", "horlicks_gap8.jpg", "horlicks_gap9.jpg", "horlicks_gap10.jpg", "horlicks_gap11.jpg", "horlicks_gap12.jpg", "horlicks_gap13.jpg", "horlicks_gap14.jpg", "horlicks_gap15.jpg", "horlicks_gap16.jpg", "horlicks_gap17.jpg", "horlicks_gap18.jpg"]},
							{KnorrKhaopayooGhoo : ["Picture1.jpg", "Picture2.jpg", "Picture3.jpg" , "Picture4.jpg" , "Picture5.jpg", "Picture6.jpg"]},
							{kelloggs : ["kellogs1.jpg", "kellogs2.jpg", "kellogs3.jpg", "kellogs4.jpg", "kellogs5.jpg", "kellogs6.jpg", "kellogs7.jpg", "kellogs8.jpg", "kellogs9.jpg", "kellogs10.jpg", "kellogs11.jpg", "kellogs12.jpg", "kellogs13.jpg", "kellogs14.jpg", "kellogs15.jpg", "kellogs16.jpg", "kellogs17.jpg", "kellogs18.jpg", "kellogs19.jpg", "kellogs20.jpg", "kellogs21.jpg"]},
				];

				$scope.credentialimages = [
							{clients : ["client1.jpg","client2.jpg","client3.jpg","client4.jpg","client5.jpg","client6.jpg","client7.jpg","client8.jpg","client9.jpg","client11.jpg","client12.jpg","client13.jpg","client14.jpg","client16.jpg","client17.jpg","client18.jpg","client20.jpg","client21.jpg","client22.jpg","client23.jpg","client24.jpg","client25.jpg","client26.jpg","client27.jpg","client28.jpg","client29.jpg","client30.jpg","client31.jpg","client32.jpg","client33.jpg","client34.jpg","client35.jpg","client36.jpg","client37.jpg","client38.jpg","client39.jpg","client40.jpg","client41.jpg","client42.jpg","client43.jpg","client44.jpg","client45.jpg","client46.jpg","client47.jpg","client49.jpg","client50.jpg","client51.jpg","client52.jpg","client53.jpg","client54.jpg","client55.jpg","client56.jpg","client57.jpg","client58.jpg","client59.jpg","client60.jpg"]},
							{awards : ["award1.jpg","award2.jpg","award3.jpg","award4.jpg","award5.jpg"]}
				];
				// Get the modal
				var modal = document.getElementById('myModal');

				// Get the image and insert it inside the modal - use its "alt" text as a caption
				var img = document.getElementById('myImg');
				var modalImg = document.getElementById("img01");
				var captionText = document.getElementById("caption");
				$scope.zoomview = function(src,pic) {
						modal.style.display = "block";
				    modalImg.src = src.concat(pic);
				}

				// Get the <span> element that closes the modal
				var span = document.getElementsByClassName("close")[0];

				// When the user clicks on <span> (x), close the modal
				// modal.onclick = function() {
				//     modal.style.display = "none";
				// }

				$(document).keydown(function(e) {
			    // ESCAPE key pressed
			    if (e.keyCode == 27) {
			        modal.style.display = "none";
			    }
			});


			$scope.pageview = function(id,text){
				var urltext = text.replace(/\s+/g, '_');
				$scope.eventid = $scope.events[id];
				$state.go('eventview', {text: urltext});
				$scope.id=id;
			};


}]);

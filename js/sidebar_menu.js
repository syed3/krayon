$("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $(".menu-span").toggle();
        $(".menu-close").toggle();
    });
    //  $("#menu-toggle-2").click(function(e) {
    //     e.preventDefault();
    //     $("#wrapper").toggleClass("toggled-2");
    //     $('#menu ul').hide();
    // });
    $("#menu-toggle-close").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
            $(".menu-span").toggle();
            $(".menu-close").toggle();
        });

    $('#menu li').click(function(){
        $(this).addClass('active');
        $(this).siblings().removeClass("active");
    });

     function initMenu() {
      $('#menu ul').hide();
      $('#menu ul').children('.current').parent().show();
      //$('#menu ul:first').show();
      $('#menu li a').click(
        function() {
          var checkElement = $(this).next();
          if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            return false;
            }
          if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $('#menu ul:visible').slideUp('normal');
            checkElement.slideDown('normal');
            return false;
            }
          }
        );
      }
    $(document).ready(function() {initMenu();});
